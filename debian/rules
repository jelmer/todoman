#!/usr/bin/make -f

# exports needed to make the tests succeed (python3-click)
# and to build reproducibly
export TZ=UTC
export LANG=C.UTF-8
export LC_ALL=C.UTF-8

# manually copy todoman/confspec.ini to build_dir/todoman/ (needed for tests)
export PYBUILD_BEFORE_BUILD=mkdir -p {build_dir}/todoman/ && cp {dir}/todoman/confspec.ini {build_dir}/todoman/
# manually build doc:
# todoman binary is manually added to PATH, as it is needed to build the doc
export PYBUILD_AFTER_BUILD=mkdir -p {build_dir}/todoman/bin && cp {dir}/bin/todo {build_dir}/todoman/bin/todoman && PATH=$(PATH):{build_dir}/todoman/bin/ \
	python3 setup.py build_sphinx -b html && \
	python3 setup.py build_sphinx -b text && \
	python3 setup.py build_sphinx -b man
# manually remove files needed for tests
export PYBUILD_AFTER_TEST=rm {build_dir}/.coverage
# Don't install Hypothesis example database into binary package.
export HYPOTHESIS_DATABASE_FILE = $(CURDIR)/debian/hypothesis

# disable dh_auto_clean as "setup.py clean" tries to download packages from the
# internet
override_dh_auto_clean:
	rm -f $(CURDIR)/debian/hypothesis

%:
	dh $@ --with python3,bash-completion --buildsystem=pybuild
